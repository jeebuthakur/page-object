package PageFactory.PageObjectPattern;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomePage extends Cap
{
	//1.  is to call the driver object from testcase to Pageobject file.
		// Concatenate driver
	public HomePage(AppiumDriver driver)
	{
	 PageFactory.initElements(new AppiumFieldDecorator(driver), this);//this keyword is reference this page.
	}
    //Below Same Step we are actully Write upper step.
	//findElementByXPath("//android.widget.TextView[@text='Preference']")
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Preference']")
	public WebElement Preferences;
	
	
}

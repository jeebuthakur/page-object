package PageFactory.PageObjectPattern;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ApiDemoAllCodes extends Cap
{
	public static void main(String[] args) throws InterruptedException, MalformedURLException
	{
		AndroidDriver<AndroidElement> driver=Capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(9000);
	
		//Constructor of the class will be invoke when you create object for the class.
		// Constructor can we defined with arguments
		//Default Constructor will be called.(This is oops Concept).
		
		//You can call the Methods or Variables of the class with class object.
		
		HomePage h=new HomePage(driver);//Directly call to HomePage
		//driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
		h.Preferences.click();//Upper vala step implement ho jaye ga.
		
		
		Preferences p=new Preferences(driver);
		//driver.findElementByXPath("//android.widget.TextView[@text='3. Preference dependencies']").click();
		p.dependencies.click();
		
		
		Dependancy d=new Dependancy(driver);
		driver.findElementByXPath("//android.widget.CheckBox[@index='0']").click();
        driver.findElementByXPath("//android.widget.TextView[@text='WiFi settings']").click();
        driver.findElementByXPath("//android.widget.EditText").sendKeys("Hello");
        driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
        
      // IF there is Find Elements
        //driver.findElementsByClassName("android.widget.Button").get(1).click();
       // @AndroidFindBy(className="//android.widget.TextView[@text='Preference']")
    	//public List<WebElement> buttons;
        //p.buttons.get(1).click();
        
}
}

package PageFactory.PageObjectPattern;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Dependancy extends Cap
{
	
	public Dependancy(AppiumDriver driver)
	{
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
	
//driver.findElementByXPath("//android.widget.CheckBox[@index='0']")
	
	@AndroidBy(xpath="//android.widget.CheckBox[@index='0']")
	public WebElement checkbox;
}
